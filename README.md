# bl-reghateiros

Code for boats built by Reghateiros-Bricolabs team.

[![pipeline status](https://gitlab.com/brico-labs/reghateiros/badges/master/pipeline.svg)](https://gitlab.com/brico-labs/reghateiros/commits/master)

## License GPL-3.0

Copyright by [J. Alberto Babío Pérez](https://github.com/jbabio/Triskel-19_Solar_RC_Boat) 

See [LICENSE](LICENSE) for details.

## Libraries

- [Arduino PID Library - Version 1.2.1](http://playground.arduino.cc/Code/PIDLibrary) MIT licensed by Brett Beauregard <br3ttb@gmail.com> brettbeauregard.com
- [FrSkySportTelemetry library](https://www.rcgroups.com/forums/showthread.php?2245978-FrSky-S-Port-telemetry-library-easy-to-use-and-configurable) "Not for commercial use" licensed by Pawelsky
- [Bolder Flight Systems SBUS](https://github.com/bolderflight/SBUS) Copyright licensed by Brian R. Taylor <brian.taylor@bolderflight.com>
- [SparkFun TB6612FNG Motor Driver Library](https://github.com/sparkfun/SparkFun_TB6612FNG_Arduino_Library) MIT licensed by Michelle @ SparkFun Electronics

## Changelog

### v1.0.0

- Platformio compilable for teensy31 board + arduino framework.
- Code imported from @jbabio Triskel-19 boat.
- Imported libraries used in Triskel-19.



