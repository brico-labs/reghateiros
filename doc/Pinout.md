
# Pinout Teensy and components

| Arduino pin | name | mode | component | purpose |
|-------------|------|------|-----------|---------|
| 0 | RXD0 | uart rx | Radio receiver | S-Bus |
| 1 | TXD0 | uart tx | Radio receiver | Smart port |
| 2 | STBY | digital output | Dual H-Bridge | Standby motor current |
| 3 | PWMA | pwm output | Dual H-Bridge | Motor A power |
| 4 | PWMB | pwm output | Dual H-Bridge | Motor B power |
| 5 | BIN2 | digital output | Dual H-Bridge | Motor B input 2 |
| 6 | BIN1 | digital output | Dual H-Bridge | Motor B input 1 |
| 7 | AIN1 | digital output | Dual H-Bridge | Motor A input 1 |
| 8 | AIN2 | digital output | Dual H-Bridge | Motor A input 2 |
| 9 | ENC1A | digital input | Encoder | Motor A encoder A |
| 10 | ENC1B | digital input | Encoder | Motor A encoder B |
| 11 | ENC2A | digital input | Encoder | Motor B encoder A |
| 12 | ENC2B | digital input | Encoder | Motor B encoder B |
| 13 | LED | digital output | Onboard LED | LED indicator |
| 14 | TEMP | one-wire bus | DS18B20 | Temperature sensor |
| 16 | HALL1 | analog input | Hall sensor | Left tail encoder |
| 17 | HALL2 | analog input | Hall sensor | Right tail encoder |
| 20 | VSEN | analog input | Power monitor | Monitored voltage |
| 21 | ISEN | analog input | Power monitor | Monitored current |
| 23 | ESCPWM | pwm output | ESC driver | Main motor power |

